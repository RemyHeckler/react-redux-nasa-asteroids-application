import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import asteroids from './asteroids';
import search from './search';

const reducers = combineReducers({
  router: routerReducer,
  asteroids,
  search,
});

export default reducers;
