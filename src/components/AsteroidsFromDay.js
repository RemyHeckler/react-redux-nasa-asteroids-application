import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const AsteroidsFromDay = props => (
  <div>
    <h5>{props.day[0]}</h5>
    {props.day[1].map((child, index) => (
      <Link to={`/details/${child.neo_reference_id}`} key={index}>
        <div>{child.name}</div>
      </Link>))
    }
  </div>
);

AsteroidsFromDay.propTypes = {
  day: PropTypes.array,
};

export default AsteroidsFromDay;
