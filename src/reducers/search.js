const initialState = '';

const filterAsteroids = (state = initialState, action) => {
  if (action.type === 'SEARCH_ASTEROIDS') {
    return action.payload;
  }
  return state;
};

export default filterAsteroids;
