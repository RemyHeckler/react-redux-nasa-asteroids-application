const initialState = {
  asteroidsList: {},
  isFetching: false,
  hasError: false,
  active: null,
};

const asteroids = (state = initialState, { type, paylod }) => {
  switch (type) {
    case 'RECEIVE_ASTEROIDS': {
      return {
        ...state,
        asteroidsList: paylod,
        isFetching: false,
        hasError: false,
        active: 0,
      };
    }
    case 'FETCH_ASTEROIDS': {
      return {
        ...state,
        asteroidsList: {},
        isFetching: true,
        hasError: false,
        active: null,
      };
    }
    case 'ERROR_ASTEROIDS': {
      return {
        ...state,
        asteroidsList: {},
        isFetching: false,
        hasError: true,
        active: null,
      };
    }
    case 'CHANGE_ACTIVE': {
      return {
        ...state,
        active: paylod,
      };
    }
    case 'REMOVE_ERROR': {
      return {
        ...state,
        hasError: false,
      };
    }
    default: return state;
  }
};

export default asteroids;
