import React from 'react';
import PropTypes from 'prop-types';

const DateForm = props => (
  <form onSubmit={(e) => { e.preventDefault(); return props.onClickGetResponse(); }} className="col-9 flex justify-center">
    <div className="flex flex-column">
      Starting date for asteroid search
      <input type="date" onChange={e => props.onChangeStartInput(e)} required />
    </div>
    <div className="col-1" />
    <div className="flex flex-column">
      Ending date for asteroid search
      <input type="date" onChange={e => props.onChangeEndInput(e)} min={props.min} max={props.max} required />
    </div>
    <div className="col-1" />
    <button type="submit">Get response</button>
  </form>
);


DateForm.propTypes = {
  onClickGetResponse: PropTypes.func,
  onChangeStartInput: PropTypes.func,
  onChangeEndInput: PropTypes.func,
  min: PropTypes.string,
  max: PropTypes.string,

};


export default DateForm;
