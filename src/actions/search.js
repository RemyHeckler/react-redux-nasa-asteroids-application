const searchAsteroids = searchValue => ({
  type: 'SEARCH_ASTEROIDS',
  payload: searchValue,
});

export default searchAsteroids;
