import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';
import createHistory from 'history/createBrowserHistory';
import { ConnectedRouter, routerMiddleware } from 'react-router-redux';
import { Route, Switch, withRouter } from 'react-router-dom';
import thunk from 'redux-thunk';
import 'basscss/css/basscss.min.css';

import reducers from './reducers';

import List from './pages/List';
import Error from './pages/Error';
import Details from './pages/Details';

import registerServiceWorker from './registerServiceWorker';

import './index.css';

const history = createHistory();
const reduxRouterMiddleware = routerMiddleware(history);

const store = createStore(reducers, applyMiddleware(thunk, reduxRouterMiddleware, createLogger()));

const Content = () => (
  <Switch>
    <Route exact path="/" component={List} />
    <Route exact path="/details/:redirectParam" component={Details} />
    <Route component={Error} />
  </Switch>
);

const ContentWithRouter = withRouter(Content);

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <ContentWithRouter />
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root'),
);

registerServiceWorker();
