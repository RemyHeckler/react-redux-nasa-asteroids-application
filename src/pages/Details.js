import React from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';

const Details = (props) => {
  const asteroid = Object.values(props.asteroid)
    .map(item => item.filter(child => child.neo_reference_id === props.match.params.redirectParam))
    .filter(item => item.length !== 0)[0];
  if (!asteroid) {
    props.push('/NotFound');
    return null;
  }
  return (
    <div>
      <header className="head">
        NASA Asteroids
      </header>
      <div className="flex flex-column m2">
        <div className="flex justify-start">
          <div className="col-3 border p2">
            Name
          </div>
          <div className="col-3 border p2">
            {asteroid[0].name}
          </div>
        </div>
        <div className="flex justify-start">
          <div className="col-3 border p2">
            Minimal diameter, meters
          </div>
          <div className="col-3 border p2">
            {asteroid[0].estimated_diameter.meters.estimated_diameter_min}
          </div>
        </div>
        <div className="flex justify-start">
          <div className="col-3 border p2">
            Max diameter, meters
          </div>
          <div className="col-3 border p2">
            {asteroid[0].estimated_diameter.meters.estimated_diameter_max}
          </div>
        </div>
      </div>
    </div>
  );
};

Details.propTypes = {
  asteroid: PropTypes.object,
  push: PropTypes.func,
};

function mapStateToProps(state) {
  return {
    asteroid: state.asteroids.asteroidsList,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({
    push,
  }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(Details);
