import React from 'react';
import PropTypes from 'prop-types';

const Pagination = props => (
  <div>
    {
      Object.keys(props.asteroidsList)
        .map((item, index) =>
          (<input
            type="button"
            key={index}
            value={index + 1}
            onClick={event => props.choseActive(event.target.value - 1)}
          />))
    }
  </div>
);

Pagination.propTypes = {
  asteroidsList: PropTypes.object,
};

export default Pagination;
