import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const AsteroidsList = props => (
  <div>
    {Object.keys(props.list)
      .map((item, ind) => (
        <div key={ind}>
          <h5 key={0}>{item}</h5>
          {props.list[item]
            .map((child, index) => (
              <Link to={`/details/${child.neo_reference_id}`} key={index}>
                <div>{child.name}</div>
              </Link>))
          }
        </div>))
    }
  </div>
);

AsteroidsList.propTypes = {
  list: PropTypes.object,
};

export default AsteroidsList;
