export const errorAsteroids = () => ({
  type: 'ERROR_ASTEROIDS',
});

export const fetchAsteroids = () => ({
  type: 'FETCH_ASTEROIDS',
});

export const receiveAsteroids = objAsteroids => ({
  type: 'RECEIVE_ASTEROIDS',
  paylod: objAsteroids,
});

export const changeActive = numberPage => ({
  type: 'CHANGE_ACTIVE',
  paylod: numberPage,
});

export const removeError = () => ({
  type: 'REMOVE_ERROR',
});
