import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import MDSpinner from 'react-md-spinner';
import PropTypes from 'prop-types';
import { push } from 'react-router-redux';

import { receiveAsteroids, fetchAsteroids, errorAsteroids, changeActive, removeError } from '../actions/asteroids';
import searchAsteroids from '../actions/search';

import SearchForm from '../components/SearchForm';
import DateForm from '../components/DateForm';
import AsteroidsFromDay from '../components/AsteroidsFromDay';
import AsteroidsList from '../components/AsteroidsList';
import Pagination from '../components/Pagination';

import '../App.css';

class List extends Component {
  static propTypes = {
    fetchAsteroids: PropTypes.func,
    receiveAsteroids: PropTypes.func,
    errorAsteroids: PropTypes.func,
    searchAsteroids: PropTypes.func,
    hasError: PropTypes.bool,
    push: PropTypes.func,
    isFetching: PropTypes.bool,
    asteroidsList: PropTypes.object,
    active: PropTypes.number,
    searchValue: PropTypes.string,
    removeError: PropTypes.func,
    changeActive: PropTypes.func,
  }

  state = {
    startDate: null,
    endDate: null,
    today: null,
    searchValue: '',
  }

  componentDidMount = () => {
    const today = new Date();
    const dd = today.getDate() < 10 ? `0${today.getDate()}` : today.getDate();
    const mm = today.getMonth() + 1 < 10 ? `0${today.getMonth() + 1}` : today.getMonth() + 1;
    const yyyy = today.getFullYear();
    this.setState({ today: `${yyyy}-${mm}-${dd}` });
  }

  componentDidUpdate = () => {
    if (this.props.hasError) {
      this.props.removeError();
      this.props.push('/NotFound');
    }
  }

  onFormSubmit = () => {
    this.props.fetchAsteroids();
    fetch('https://api.nasa.gov/neo/rest/v1/feed?start_date=' +
        `${this.state.startDate}&end_date=${this.state.endDate}` +
        '&api_key=ukVq5SnuQCTDmqCZg2ur4MULNyDh08nH8bIYkapf')
      .then(res => res.json())
      .then((res) => {
        if (res.http_error) {
          throw new Error();
        } else {
          return this.props.receiveAsteroids(res.near_earth_objects);
        }
      })
      .catch(this.props.errorAsteroids);
  }

  handleChangeSearch = () => {
    this.props.searchAsteroids(this.state.searchValue);
  }

  render() {
    return (
      <div className="App">
        <header className="head">
          NASA Asteroids
        </header>
        <div className="flex justify-start mt2">
          <DateForm
            onClickGetResponse={() => this.onFormSubmit()}
            onChangeStartInput={e => this.setState({ startDate: e.target.value })}
            onChangeEndInput={e => this.setState({ endDate: e.target.value })}
            min={this.state.startDate}
            max={this.state.today}
          />
          <SearchForm
            onChangeSearchInput={e => this.setState({ searchValue: e.target.value })}
            onClickSearch={() => this.handleChangeSearch()}
          />
        </div>
        <div className="m2">
          {
            !Object.keys(this.props.asteroidsList).length || this.props.searchValue ? null
            :
            <Pagination
              asteroidsList={this.props.asteroidsList}
              choseActive={index => this.props.changeActive(index)}
            />
          }
        </div>
        <div>
          {this.props.isFetching ? <MDSpinner className="mt2" /> : null}
        </div>
        <div>
          {
            !Object.keys(this.props.asteroidsList).length ? null :
            this.props.searchValue ? <AsteroidsList list={this.props.asteroidsList} /> :
            <AsteroidsFromDay day={Object.entries(this.props.asteroidsList)[this.props.active]} />
          }
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    asteroidsList: !state.search ? state.asteroids.asteroidsList :
      Object.entries(state.asteroids.asteroidsList)
        .map(item => [item[0], item[1].filter(ast => ast.name.includes(state.search))])
        .filter(item => item[1].length !== 0)
        .reduce((acc, x) => ({ ...acc, [x[0]]: x[1] }), {}),
    isFetching: state.asteroids.isFetching,
    hasError: state.asteroids.hasError,
    active: state.asteroids.active,
    searchValue: state.search,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({
    receiveAsteroids,
    fetchAsteroids,
    errorAsteroids,
    searchAsteroids,
    changeActive,
    removeError,
    push,
  }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(List);
