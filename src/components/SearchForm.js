import React from 'react';
import PropTypes from 'prop-types';

const SearchForm = props => (
  <form onSubmit={(e) => { e.preventDefault(); return props.onClickSearch(); }} className="col-3">
    <input type="text" onChange={e => props.onChangeSearchInput(e)} />
    <button type="submit">Search</button>
  </form>
);

SearchForm.propTypes = {
  onClickSearch: PropTypes.func,
  onChangeSearchInput: PropTypes.func,
};

export default SearchForm;
